const mongoose = require('mongoose');

//setting up object/model for each 'game'
const gameSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    date: Date,
    playerOneScore: Number,
    playerTwoScore: Number,
    playerOneName: String,
    playerTwoName: String,
    winner: String
})

//model name and schema as args
module.exports = mongoose.model('Game', gameSchema);