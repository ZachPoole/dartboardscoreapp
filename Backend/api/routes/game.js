const express = require('express');
const mongoose = require('mongoose');

//importing data model
const Game = require('../models/game')


// a way to package routes together under one name
const router = express.Router();


// get all game data from database
router.get('/', (req, res, next) => {

    //returns all games in the database
    Game.find()
    .exec()
    .then(doc => {
        console.log(doc);

        //move res.status here because of async
        res.status(200).json(doc);
    })
    .catch(err => {
        console.log(err);

        //move res.status here because of async
        res.status(500).json({
            error: err
        });
    });
});



// post game data to database
router.post('/', (req, res, next) => {

    const game = new Game({
        _id: new mongoose.Types.ObjectId(),
        date: new Date(),
        playerOneScore: req.body.playerOneScore,
        playerTwoScore: req.body.playerTwoScore,
        playerOneName: req.body.playerOneName,
        playerTwoName: req.body.playerTwoName,
        winner: req.body.winner
    });

    //method provided by mongoose to save data 
    game.save().then(result => {
        console.log(result);
        res.status(200).json({
            message: "game added to collection"
        })
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            message: "game not added to collection"
        })
    });

});



module.exports = router;

