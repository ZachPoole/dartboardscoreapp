import Home  from './Home';
import SignLog from './SignLog';
import About from './About';
import Game from './Game';

export {Home, SignLog, About, Game};