import React from 'react';

//Is it passed in from a parent via props? If so, it probably isn’t state.
//Does it remain unchanged over time? If so, it probably isn’t state.
//Can you compute it based on any other state or props in your component? If so, it isn’t state.

export default function Home () {
    return (
        <div>
            <h1>Home Page</h1>
        </div>
    );
}