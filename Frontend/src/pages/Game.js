import React from 'react';
import {Cricket} from '../components/index';

export default function Game() {
    return (
        <div>
            <h1>Game Page</h1>
            <Cricket />
        </div>
    );
}