import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import  {Home, SignLog, About, Game} from './pages/index';
import { Navbar } from './components/index';
import './App.css';

export default function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route exact path="/"><Home /></Route>
          <Route path="/signlog"><SignLog /></Route>
          <Route path="/about"><About /></Route>
          <Route path="/game"><Game /></Route>
          <Route>Page Doesn't Exist</Route>
        </Switch>
      </Router>
    </>
  );
}

