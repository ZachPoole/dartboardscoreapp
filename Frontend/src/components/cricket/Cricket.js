import React, { useState } from 'react';
import styled from 'styled-components';
import { NumberButton } from '../index';

const Grid = styled.div`
    display: grid;
    grid-template-columns: repeat(${props => props.cols}, auto);
    grid-template-rows: ${props => props.rowHeight};
`;

export default function Cricket () {
    
    class Player {
        constructor(pName, pNum) {
            this.playerName = pName;
            this.playerNum = pNum;
            this.totalScore = 0;
        }
    }


    let playerOne = new Player('Zach', 1);
    let playerTwo = new Player('Nick', 2);


    const [playerTurn, setPlayerTurn] = useState(1);

    const [playerOneScore, setPlayerOneScore] = useState(0);
    const [playerTwoScore, setPlayerTwoScore] = useState(0);

    const [playerOneNums, setPlayerOneNums] = useState({
        15: 0,
        16: 0,
        16: 0,
        17: 0,
        18: 0,
        19: 0,
        20: 0,
        25: 0   
    });

    const [playerTwoNums, setPlayerTwoNums] = useState({
        15: 0,
        16: 0,
        16: 0,
        17: 0,
        18: 0,
        19: 0,
        20: 0,
        25: 0   
    })


    function updateScore(num) {
        if (playerTurn === 1) {
            
            //if p1 tallies < 3, add 1 to tally
            if (playerOneNums[num] < 3) {
                setPlayerOneNums({...playerOneNums, [num]: playerOneNums[num] + 1});

            // since p1 tallt is > 3, check if p2 tallies are < 3, if they are add points
            } else if (playerTwoNums[num] < 3) {
                setPlayerOneNums({...playerOneNums, [num]: playerOneNums[num] + 1});
                setPlayerOneScore(playerOneScore + num);

            } else {
                alert('Player Two has Already Closed that Number');
            }


        } else {

            //if p2 tallies < 3, add 1 to tally
            if (playerTwoNums[num] < 3) {
                setPlayerTwoNums({...playerTwoNums, [num]: playerTwoNums[num] + 1});

            // since p2 tallt is > 3, check if p1 tallies are < 3, if they are add points
            } else if (playerOneNums[num] < 3) {
                setPlayerTwoScore(playerTwoScore + num);
                setPlayerTwoNums({...playerTwoNums, [num]: playerTwoNums[num] + 1});

            } else {
                alert('Player One has Already Closed that Number');
            }

        }
    }

    function toggleTurn() {
        setPlayerTurn(playerTurn === 1 ? 2 : 1);
    };

    return (
        <>
        <h1 style={{textAlign: 'center'}}>Player {playerTurn}'s Turn!</h1>
        <Grid cols={3} rows={'auto'} >
            <div>
                <h1>Player 1: Zach</h1>
                <h1>{playerOneScore}</h1>
            </div>
            <div style={{margin: '0 0'}}>
                {Object.getOwnPropertyNames(playerOneNums).map(num => <NumberButton updateScore={() => updateScore(parseInt(num))} playerOneNumTallies={playerOneNums[num]} playerTwoNumTallies={playerTwoNums[num]} currentNum={num} />)}
            </div>
            <div>
                <h1>Player 2: Nick</h1>
                <h1>{playerTwoScore}</h1>
            </div>
        </Grid>
        <div style={{fontSize: "2em", border: "1px solid black", padding: 10}} onClick={toggleTurn}>Finish Turn</div>
        </>
    );
}