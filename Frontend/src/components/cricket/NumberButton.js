import React from 'react';
import styled from 'styled-components';

const NumButton = styled.div`
    padding: 3% 5em;
    border: 1px solid black;
    width: 100%;
    text-align: center;
    margin: 2% 0;



    &:hover {
        background-color: gray;
    }
`;

const Grid = styled.div`
    display: grid;
    grid-template-columns: repeat(${props => props.cols}, auto);
    grid-template-rows: ${props => props.rowHeight};
    justify-items: center;
    align-items: center;
    font-size: 2em;
`;




export default function NumberButton (props) {
    return(
        <Grid cols={3} rows={'auto'}>
            <div>{props.playerOneNumTallies}</div>
            <div><NumButton onClick={() => props.updateScore(props.currentNum)}>{props.currentNum}</NumButton></div>
            <div>{props.playerTwoNumTallies}</div>
        </Grid>
    );
}