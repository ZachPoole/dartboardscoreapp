import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';


const Grid = styled.div`
    display: grid;
    grid-template-columns: repeat(${props => props.cols}, auto);
    grid-template-rows: ${props => props.rowHeight};
`;


const NavGrid = styled(Grid)`
    background-color: #434343;
    color: white;
`;


const GridColItem = styled.div`
    grid-column: ${props => props.colStart} / ${props => props.colEnd};
`;


const StyledLink = styled(Link)`
    color: white;
    width: 80%;
    padding: 9% 0;
    text-align: center;
    justify-self: start;
    align-self: center;

    &:hover {
        background-color: #666;
        transition: background-color 500ms
    };
`;


const Title = styled(GridColItem)`
    justify-self: start;
    align-self: center;
    padding-left: 2%;
    cursor: default;
`;


export default function Navbar() {


    return (
        <NavGrid cols={2} rowHeight={'3em'}>
            <Title colStart={1} colEnd={2}>Dartboard Score Tracker</Title>
            <GridColItem colStart={2} colEnd={3}>
                <Grid cols={6} rowHeight={'3em'}>
                    <StyledLink to="/">Home</StyledLink>
                    <StyledLink to="/about">About</StyledLink>
                    <StyledLink to="/game">Game</StyledLink>
                </Grid>
            </GridColItem>
        </NavGrid>
    );
}